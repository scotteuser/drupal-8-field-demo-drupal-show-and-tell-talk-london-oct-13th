# Drupal 8 Field Demo

This repo is a simple module to demonstrate the use of the Drupal 8 Field API
initially put together for the Drupal Show and Tell London, Oct 13th, 2016.

## Why create your own field?

Alternatives

*  Inline entity form
*  Paragraph type
*  Field collection

Why a field

*  Data with no value on its own - tie together data that is not useful without it's counterparts (eg, latitude without a longitude, car model without it's make).
*  Editor experience - out of the box perhaps more simple.
*  Database performance - compared to a field collection or paragraph type, this is less database queries as loading all values of the field is a single query while a paragraph type with 3 fields would be 4 queries.

## The classes involved

Field Type

*  This controls what happens in the Manage Fields tab
*  This controls the database schema for each created field of this type
*  This sets out the options for site builder in terms of how the data is stored (think varchar vs text or blob, or tinyint vs smallint vs int, etc)

Field Widget

*  This controls what happens in the Manage Form Display tab
*  This controls the display of Form Elements on entity edit for instance
*  This sets out the options for the site builder in terms of how those form elements are displayed on the entity edit form

Field Formatter

*  This gives you full control over the front-end render array, potentially using built-in render types
or your own `#theme`
*  This sets out the options for the site builder in terms of how the user entered gets displayed on the
front-end

## Drupal Console

You can generate boilerplate code with the following
drupal console commands:

*  `drupal generate:plugin:field`
*  `drupal generate:plugin:fieldtype`
*  `drupal generate:plugin:fieldwidget`
*  `drupal generate:plugin:fieldformatter`

## Examples

Core examples

*  File
*  Image
*  etc

Contrib examples

*  Geolocation
*  Address
*  Video embed
*  etc
