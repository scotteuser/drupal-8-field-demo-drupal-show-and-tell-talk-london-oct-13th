<?php

namespace Drupal\ratings_demo\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rating_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "rating_formatter_type",
 *   label = @Translation("Rating formatter"),
 *   field_types = {
 *     "rating_field_type"
 *   }
 * )
 */
class RatingFormatterType extends FormatterBase {

  /**
   * Default settings for the Manage Display tab.
   */
  public static function defaultSettings() {
    return [
      'rating_display' => 'percentage',
    ] + parent::defaultSettings();
  }

  /**
   * Settings form using Form API for the Manage Display Tab.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['rating_display'] = array(
      '#title' => t('Rating display'),
      '#type' => 'radios',
      '#options' => array(
        'percentage' => t('Percentage'),
        'none' => t('None'),
      ),
      '#default_value' => $this->getSetting('rating_display'),
      '#description' => t('How to display the calculated rating.'),
      '#required' => TRUE,
    );
    return $form + parent::settingsForm($form, $form_state);
  }

  /**
   * This text is shown on the Manage Display page.
   *
   * Before the display settings for the field are expanding, this text
   * would be shown to the site builder.
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Rating display: @display', array('@display' => $this->getSetting('rating_display')));
    return $summary;
  }

  /**
   * The front end display of the data.
   *
   * You can use anything available in the Render API including something
   * with your own theme using #theme or your own js or css using #attached.
   * In this case, we display the data using the built in table render array.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = [];

    // Build a table render array.
    $build['ratings'] = [
      '#type' => 'table',
      '#header' => [t('Ratings'), t('Upvotes'), t('Downvotes')],
      '#empty' => t('This has not yet been rated.'),
    ];

    // Maybe add the percentage header.
    $display = $this->getSetting('rating_display');
    if ($display == 'percentage') {
      $build['ratings']['#header'][] = t('Total');
    }

    foreach ($items as $delta => $item) {

      // Row label.
      $build['ratings'][$delta]['label'] = [
        '#plain_text' => $item->label,
      ];

      // Up and downvote columns.
      $build['ratings'][$delta]['upvotes'] = [
        '#plain_text' => $item->upvotes,
      ];
      $build['ratings'][$delta]['downvotes'] = [
        '#plain_text' => $item->downvotes,
      ];

      // Percentage.
      if ($display == 'percentage') {
        $count = $item->upvotes + $item->downvotes;
        if ($count) {
          $percentage = ($item->upvotes / $count);
          $total = round(($percentage * 100), $item->rounding);
          $total .= t('%');
        }
        else {
          $total = t('N/A');
        }

        $build['ratings'][$delta]['total'] = [
          '#plain_text' => $total,
        ];
      }
    }

    return [$build];
  }

}
