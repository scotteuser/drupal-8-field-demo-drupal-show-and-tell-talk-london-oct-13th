<?php

namespace Drupal\ratings_demo\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'rating_field_type' field type.
 *
 * @FieldType(
 *   id = "rating_field_type",
 *   label = @Translation("Rating field"),
 *   description = @Translation("A field to show ratings."),
 *   default_widget = "rating_widget_type",
 *   default_formatter = "rating_formatter_type"
 * )
 */
class RatingFieldType extends FieldItemBase {

  /**
   * The default values for storage settings.
   *
   * This controls what the prefilled values for the storage settings
   * should be in the storageSettingsForm() function. The default storage
   * settings helps future-proof this, but at the moment in core is empty.
   */
  public static function defaultStorageSettings() {
    return [
      'max_digits' => 3,
    ] + parent::defaultStorageSettings();
  }

  /**
   * The Form API form to control the storage settings.
   *
   * Here you put any controls you want to give to the site editor over the
   * way the data is stored in the database. Eg, a textfield might be 255
   * length for VARCHAR or longer it needs to be TEXT.
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['max_digits'] = [
      '#type' => 'number',
      '#title' => t('Maximum digits'),
      '#default_value' => $this->getSetting('max_digits'),
      '#required' => TRUE,
      '#description' => t('The maximum number of digits for upvotes and downvotes.'),
      '#min' => 1,
      '#max' => 18,
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * Use the Symfony Constraint Validator.
   *
   * This uses the Symfony Constraint Validator to ensure the data the
   * visitor adds won't be rejected by the database when it is being inserted.
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    if ($max_digits = $this->getSetting('max_digits')) {
      $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
      $constraints[] = $constraint_manager->create('ComplexData', [
        'upvotes' => [
          'Length' => [
            'max' => $max_digits,
            'maxMessage' => t('%name: may not be longer than @max digits.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max' => $max_digits,
            ]),
          ],
        ],
        'downvotes' => [
          'Length' => [
            'max' => $max_digits,
            'maxMessage' => t('%name: may not be longer than @max digits.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max' => $max_digits,
            ]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * Set up the typed data for the fields.
   *
   * Use the typed data API getter and setter methods to let Drupal know
   * about the data that this field will store.
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['label'] = DataDefinition::create('string')
      ->setLabel(t('Rounding'));

    $properties['upvotes'] = DataDefinition::create('integer')
      ->setLabel(t('Upvotes'));

    $properties['downvotes'] = DataDefinition::create('integer')
      ->setLabel(t('Downvotes'));

    $properties['rounding'] = DataDefinition::create('integer')
      ->setLabel(t('Rounding'));

    return $properties;
  }

  /**
   * The database structure for each field table.
   *
   * Drupal will automatically add the fields like entity type, language,
   * ID, and deleted. Max digits should really handle tiny, small, medium,
   * etc int sizes, but for demo purposes...
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $vote_size = ($field_definition->getSetting('max_digits') < 3 ? 'tiny' : 'big');
    $schema = [
      'columns' => [
        'label' => [
          'description' => 'The name of the rating',
          'type' => 'varchar',
          'length' => 255,
        ],
        'upvotes' => [
          'type' => 'int',
          'size' => $vote_size,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Stores the number of upvotes',
        ],
        'downvotes' => [
          'type' => 'int',
          'size' => $vote_size,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Stores the number of downvotes',
        ],
        'rounding' => [
          'type' => 'int',
          'size' => 'small',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'Number of decimal places to round to',
        ],
      ],
      'indexes' => [
        'label' => ['label'],
        'upvotes' => ['upvotes'],
        'downvotes' => ['downvotes'],
      ],
    ];

    return $schema;
  }

  /**
   * Generate sample values.
   *
   * This is used when a site builder wants to generate sample contents. We
   * can use Drupal's Random utility class to help us generate some words
   * and built in php for random integers.
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $random = new Random();
    $values['label'] = $random->word(mt_rand(1, 255));

    $max_digits = $field_definition->getSetting('max_digits');
    $vote_max = ($max_digits < 3 ? 9 : 999);
    $values['upvotes'] = rand(0, $vote_max);
    $values['downvotes'] = rand(0, $vote_max);

    $values['rounding'] = rand(0, 2);
    return $values;
  }

  /**
   * Criteria for when this field should be considered empty.
   *
   * In this example, evening if rounding is set, we consider it empty, we only
   * consider this not empty if there is a value for up or down votes.
   */
  public function isEmpty() {
    $upvotes = $this->get('upvotes')->getValue();
    $downvotes = $this->get('downvotes')->getValue();
    return $upvotes === NULL || $upvotes === '' || $downvotes === NULL || $downvotes === '';
  }

}
