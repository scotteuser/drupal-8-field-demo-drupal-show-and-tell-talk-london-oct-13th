<?php

namespace Drupal\ratings_demo\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rating_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "rating_widget_type",
 *   label = @Translation("Rating widget"),
 *   field_types = {
 *     "rating_field_type"
 *   }
 * )
 */
class RatingWidgetType extends WidgetBase {

  /**
   * The default settings in the Manage Form Display tab.
   */
  public static function defaultSettings() {
    return [
      'show_rounding' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * The form using the Form API in the Manage Form Display tab.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['show_rounding'] = [
      '#type' => 'checkbox',
      '#title' => t('Show rounding'),
      '#default_value' => $this->getSetting('show_rounding'),
      '#description' => $this->t('Whether or not to show controls over rounding.'),
    ];

    return $elements;
  }

  /**
   * This text is shown on the Manage Form Display page.
   *
   * Before the display settings for the field are expanding, this text
   * would be shown to the site builder.
   */
  public function settingsSummary() {
    if ($this->getSetting('show_rounding')) {
      $rounding = $this->t('Yes');
    }
    else {
      $rounding = $this->t('No');
    }

    return [
      $this->t('Show rounding? @rounding', ['@rounding' => $rounding]),
    ];
  }

  /**
   * The form shown to the site editor or author.
   *
   * This is what the site editor would see when creating, for instance, a
   * node. They would enter their data here. In this case, we wrap the form
   * elements in the form--inline class to keep the form compact.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['#prefix'] = '<div class="form--inline">';

    $element['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline'],
      ],
    ];

    $element['label'] = [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->label) ? $items[$delta]->label : NULL,
      '#maxlength' => 255,
      '#title' => $this->t('Label'),
    ];

    $element['upvotes'] = [
      '#type' => 'number',
      '#default_value' => isset($items[$delta]->upvotes) ? $items[$delta]->upvotes : NULL,
      '#min' => 0,
      '#title' => $this->t('Upvotes'),
    ];

    $element['downvotes'] = [
      '#type' => 'number',
      '#default_value' => isset($items[$delta]->downvotes) ? $items[$delta]->downvotes : NULL,
      '#min' => 0,
      '#title' => $this->t('Downvotes'),
    ];

    if ($this->getSetting('show_rounding')) {
      $element['rounding'] = [
        '#type' => 'number',
        '#default_value' => isset($items[$delta]->rounding) ? $items[$delta]->rounding : NULL,
        '#placeholder' => $this->getSetting('show_rounding'),
        '#min' => 0,
        '#title' => $this->t('Rounding'),
      ];
    }

    $element['#suffix'] = '</div>';

    return $element;
  }

}
